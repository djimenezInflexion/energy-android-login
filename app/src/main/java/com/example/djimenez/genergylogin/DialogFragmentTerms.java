package com.example.djimenez.genergylogin;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Daniel on 08/06/2016.
 */
public class DialogFragmentTerms extends DialogFragment {

    //objects
    private TextView tvClose = null;

    public DialogFragmentTerms() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_terms, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        tvClose = (TextView) rootView.findViewById(R.id.tv_dismiss);
        tvClose.setOnClickListener(closeDialog);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    View.OnClickListener closeDialog = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            dismiss();
        }
    };

}
