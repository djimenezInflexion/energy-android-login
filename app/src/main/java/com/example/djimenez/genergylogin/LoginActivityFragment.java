package com.example.djimenez.genergylogin;



import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A placeholder fragment containing a simple view.
 */
public class LoginActivityFragment extends Fragment{


    //objetos
    private EditText etCorreo = null;
    private EditText etContraseña = null;
    private Button btnIniciar = null;
    private LinearLayout llnotReg = null;
    private LinearLayout llrecPass = null;
    private LinearLayout llshowTerms = null;

    //cadenas contraseña y usuario de los edit text
    private String correo = null;
    private String contrasena = null;

    //url para login
    private String URL = "http://pruebagenergy.azurewebsites.net/Token";
    private String token = null;

    //string expresion regular email
    private static final String EXP_REG_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    public LoginActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        //relacion de objetos con elementos del fragment
        etContraseña = (EditText) rootView.findViewById(R.id.et_contrasena);
        etCorreo = (EditText) rootView.findViewById(R.id.et_correo);
        btnIniciar = (Button) rootView.findViewById(R.id.btn_iniciar);
        btnIniciar.setOnClickListener(iniciarSesion);

        llnotReg = (LinearLayout) rootView.findViewById(R.id.linearl_not_reg);
        llnotReg.setOnClickListener(showPopup);

        llrecPass = (LinearLayout) rootView.findViewById(R.id.linearl_rec_pass);
        llrecPass.setOnClickListener(showDialog);

        llshowTerms = (LinearLayout) rootView.findViewById(R.id.layout_terms);
        llshowTerms.setOnClickListener(showTerms);



        return rootView;
    }

    private  View.OnClickListener iniciarSesion = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            correo = etCorreo.getText().toString();
            contrasena = etContraseña.getText().toString();
            login();
        }
    };

    private View.OnClickListener showPopup = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            Toast.makeText(getActivity().getApplicationContext(), "Contacta a G-Energy" +
                    " 01-800-123-456", Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener showDialog = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            FragmentManager fm = getFragmentManager();
            DialogFragmentRecPass dialogRP = new DialogFragmentRecPass();
            dialogRP.show(fm, "DialogFragmentRecPass");
        }
    };

    private View.OnClickListener showTerms = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            FragmentManager fm = getFragmentManager();
            DialogFragmentTerms dialogFT = new DialogFragmentTerms();
            //dialogFT.show(fm, "DialogFragmentTerms"); //se inhabilito esta funcion por que no permitia la comunicacion
            //crea el dialog con funcion padre e hijo para lograr comunicacion desde el fragment destino
            //dialogFT.show(LoginActivityFragment.this.getChildFragmentManager(), "DialogFragmentTerms");
            dialogFT.show(fm, "DialogTerms");
        }
    };

    //metodo chekea el cBox de los terminos

    //login para obtener el access token al loguearse
    public void loginUser(String url){
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        StringRequest strRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            token = jsonResponse.getString("access_token");
                            Toast.makeText(getActivity().getApplicationContext(), "ACCESS TOKEN: " + token, Toast.LENGTH_SHORT).show();
                            loginCorrecto();

                        } catch (Throwable tx) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(getActivity().getApplicationContext(), "Revisa tu correo y contraseña", Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", correo);
                params.put("password", contrasena);
                params.put("grant_type", "password");
                return params;
            }
        };
        queue.add(strRequest);
    }

    //metodo que valida el email
    public static boolean validateEmail(String email){
        //compila la expresion regular en un pattern
        Pattern pat = Pattern.compile(EXP_REG_EMAIL);
        //compara la cadena dada con la expresion regular
        Matcher mat = pat.matcher(email);
        return mat.matches();
    }

    //metodo que valida que los campos no esten vacios
    public boolean validaCamposVacios(){
        if (correo.isEmpty()){
            Toast.makeText(getActivity().getApplicationContext(), "Ingresa tu correo", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            if(contrasena.isEmpty()){
                Toast.makeText(getActivity().getApplicationContext(), "Ingresa tu contraseña", Toast.LENGTH_SHORT).show();
                return false;
            }else{
                //si los dos campos estan llenos regresa true para continuar con las demas validaciones
                return true;
            }
        }
    }

    //metodo que realiza el login, obteniendo validaciones de los demas metodos
    public void login(){
        if(validaCamposVacios()){
                if(validateEmail(correo)){
                    loginUser(URL);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "Correo invalido", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public void loginCorrecto(){

        Intent intent = new Intent(getActivity(), ActivityVerifyData.class);
        startActivity(intent);

    }



}
