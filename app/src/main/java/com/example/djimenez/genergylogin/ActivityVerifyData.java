package com.example.djimenez.genergylogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ActivityVerifyData extends AppCompatActivity {

    //objetos
    EditText etPass = null;
    EditText etPassconf = null;
    Button btnEntrar = null;
    LinearLayout layoutPass1 = null;
    LinearLayout layoutPass2 = null;
    ImageView imagePass1 = null;
    ImageView imagePass2 = null;

    //cadenas que tienen las contraseñas
    private String pass = null;
    private String passConf = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_data);

        //relacion de objetos con vistas
        etPass = (EditText) findViewById(R.id.et_new_pass);
        etPassconf = (EditText) findViewById(R.id.et_new_pass_conf);
        layoutPass1 = (LinearLayout) findViewById(R.id.layout_pass1);
        layoutPass2 = (LinearLayout) findViewById(R.id.layout_pass2);
        imagePass1 = (ImageView) findViewById(R.id.image_view_pass1);
        imagePass2 = (ImageView) findViewById(R.id.image_view_pass2);
        btnEntrar = (Button) findViewById(R.id.btn_entrar);
        btnEntrar.setOnClickListener(entrar);
    }




    private View.OnClickListener entrar = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            pass = etPass.getText().toString();
            passConf = etPassconf.getText().toString();
            validaTodo();

        }
    };

    private boolean validaCamposVacios(){

        if (pass.isEmpty()){
            Toast.makeText(this, "Ingresa tu nueva contraseña", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            if (passConf.isEmpty()){
                Toast.makeText(this, "Confirma tu contraseña", Toast.LENGTH_SHORT).show();
                return false;
            }
            else{
                return true;
            }
        }
    }

    private boolean validaPass(){
        if (pass.equals(passConf)){
            return true;
        }else{
            Toast.makeText(this,"Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            layoutPass1.setBackgroundResource(R.drawable.border_layout_red);
            layoutPass2.setBackgroundResource(R.drawable.border_layout_red);
            imagePass1.setImageResource(R.drawable.incorrect);
            imagePass2.setImageResource(R.drawable.incorrect);
            return false;
        }
    }

    private void validaTodo(){
        if (validaCamposVacios()){
            if (validaPass()){
                Toast.makeText(this, "Mensaje DEBUG: Contraseña cambiada", Toast.LENGTH_SHORT).show();
                layoutPass1.setBackgroundResource(R.drawable.border_layout_green);
                layoutPass2.setBackgroundResource(R.drawable.border_layout_green);
                imagePass1.setImageResource(R.drawable.correct);
                imagePass2.setImageResource(R.drawable.correct);
            }
        }

    }



}
