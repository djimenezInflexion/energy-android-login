package com.example.djimenez.genergylogin;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Text;

/**
 * Created by djimenez on 07/06/16.
 */
public class DialogFragmentRecPass extends DialogFragment {

    //url recover pssword
    private static String URL = "http://pruebagenergy.azurewebsites.net/api/Account/RecoverPassword?email=";

    private EditText etEmail = null;
    private TextView tvClose = null;
    private Button btnSendEmail = null;
    //var string que contiene el email del usuario
    private String email = null;

    public DialogFragmentRecPass() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_rec_pass, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        etEmail = (EditText) rootView.findViewById(R.id.et_email);
        tvClose = (TextView) rootView.findViewById(R.id.tv_close);
        tvClose.setOnClickListener(closeDialog);
        btnSendEmail = (Button) rootView.findViewById(R.id.btn_send_email);
        btnSendEmail.setOnClickListener(sendEmail);

        return rootView;
    }

    View.OnClickListener closeDialog = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            dismiss();
        }
    };

    View.OnClickListener sendEmail = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            email = etEmail.getText().toString();
            rec_pass(URL + email);
        }
    };

    //metodo para el get de recover pass
    void rec_pass(String url){

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //tvMessage.setText("Response is: "+ response);
                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        //dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //tvMessage.setText("That didn't work!");
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    //validar email con una expresion regular

}
